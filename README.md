# Auto Pic Bot

Auto Pic Bot, a simple Telegram bot in python 3.9 that takes a pictures from a folder, send it to a channel/user, and move it to another folder or delete it.

## Setup

### Without virtual environment

1. Import the requirements with pip:

```sh
pip install -r requirements.txt
```

2. Configure the bot (See [Configuration](#configuration))

3. Start the bot by executing `app.py` :

```sh
python src/app.py
```

### With virtual environment

0. _Edit the makefile to your local python 3.9 binary if needed_

1. You can use the makefile to automate the venv setup :

```sh
make init
```

2. Configure the bot (See [Configuration](#configuration))

3. Start the bot :

```sh
make run
```

## Configuration

Fill the `config.toml` with the help of the `login.py` script :

```sh
python login.py
```

You can also do it manually with the help from `config_example.toml`.

## Dev

### To-do :

- Add some mockify stuff to the tests for functional testing

### Done :

- add a "repost mode" that use pictures in the moved folder in case the posting folder is empty (only in moved mode)
- adding support to send videos in gif animations
- make the bot regularly send pics without a service
- possibility to delete the file instead of moving them
- ask the bot how many images/videos are left in the folder
- Add /help function to the bot
- Easy to read configuration in TOML format
- login script to help finding chat id's and seting up base configuration
