# Path to system python 3.9 executable
PYTHON_SYS_BIN := /usr/bin/python3.9

# Path to the VENV directory
PYTHON_VENV  := $(shell pwd)/venv

# Utility vars
PIP_BIN := ${PYTHON_VENV}/bin/pip
PYTHON_BIN := ${PYTHON_VENV}/bin/python


# Initialise Virtual env and install required libraries
.PHONY: init
init:
	@if [ ! -e ${PYTHON_VENV} ]; then \
		echo -e "\033[1;32mInitialising VENV...\033[0m\n"; \
		${PYTHON_SYS_BIN} -m venv ${PYTHON_VENV}; \
	fi
	@echo -e "\033[1;32mUpgrading PIP and installing/upgrading requirements...\033[0m"
	${PIP_BIN} install -q --upgrade pip
	${PIP_BIN} install -q -r ./requirements.txt

# Run available tests on this project
.PHONY: test
test: init
	@echo -e "\n\033[1;32mRunning tests...\033[0m"
	mkdir -p ./tests/test_files/repost
	cd ./tests && PYTHONPATH="$(shell pwd)/tests:$(shell pwd)/src" ${PYTHON_BIN} -m pytest

# Run the bot
.PHONY: run
run: init
	@echo -e "\n\033[1;32mStarting auto-pic bot...\033[0m"
	${PYTHON_BIN} src/app.py

# Clean test files
.PHONY: clean-tests
clean-tests:
	@echo -e "\033[1;32mCleaning test folders...\033[0m"
	@find . -regex '^\./\(src\|test\).*\__pycache__$$' | xargs --no-run-if-empty -t rm -Rf
	rm -Rf ./.pytest_cache

# Remove virtual env folder and clean test files
.PHONY: clean
clean: clean-tests
	@echo -e "\n\033[1;32mCleaning VENV...\033[0m"
	rm -Rf ${PYTHON_VENV}
