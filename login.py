""" login script to log in to the correct chat """
import os
import shutil
import sys
import toml
import requests
from getpass import getpass

CONFIG_TOML = "config.toml"

def main():
    """main function of the login for autopicbot"""
    if os.path.exists(CONFIG_TOML):
        if os.path.isfile(CONFIG_TOML):
            print("{} already exist, renaming it {}.bak".format(CONFIG_TOML,
                                                                CONFIG_TOML))
            shutil.move(CONFIG_TOML, CONFIG_TOML + ".bak")
        elif os.path.isdir(CONFIG_TOML):
            print("{} is a directory, exiting login".format(CONFIG_TOML))
            sys.exit(1)

    token = input("bot token: ")
    url = "https://api.telegram.org/bot" + token + "/getUpdates"
    #TODO: request api ? Or something else ?
    req = requests.get(url)
    rjson = req.json()
    ids  = {}
    if not rjson['ok']:
        print("Error: not getting update messages from the bot")
        sys.exit(1)
    for update in rjson['result']:
        if 'message' in update:
            info = update['message']['chat']
            if info['type'] == "private":
                ids[info['id']] = (info['username'], info['type'])
            else:
                ids[info['id']] = (info['title'], info['type'])

        elif 'channel_post' in update:
            info = update['channel_post']['chat']
            ids[info['id']] = (info['title'], info['type'])

    ids_list = list(ids.items())
    print("Choose one chat from the following to post pictures")
    for i, (k, v) in enumerate(ids_list):
        print("{}: {}".format(i + 1, v));
    print("If you can't find your chat, verify it is indeed in the group/channel, and send a message to get updates")

    if len(ids_list) == 0:
        print("No chat found, exiting")
        sys.exit(1)

    choice = int(input("Chat: "))
    if choice <= 0 and choice > len(ids_list):
        print("Error, {} isn't a correct choice".format(choice))
        sys.exit(1)

    chat_id = str(ids_list[choice - 1][0])
    print(chat_id)

    time_interval = int(input("Choose a time interval between 2 pics (in seconds): "))

    src_folder = input("Enter the full path of the directory to post picture for: ")
    exclude_folder = input("Enter the full path of the directory to exclude error files to: ")
    move_folder = input("Enter the full path of the directory to move picture to (empty if you want to just delete the pictures): ")
    delete_mode = move_folder == ""
    repost_mode = False
    if not delete_mode: 
        repost_mode_str = input("Do you want to post picture from the move folder [y/N]: ")
        repost_mode = repost_mode_str.lower() == "y"

    toml_dict = {
            "telegram_API": {
                    "token": token,
                    "chat_id": chat_id,
                    "time_interval": time_interval,
                },
            "paths": {
                    "src_folder": src_folder,
                    "exclude_folder" : exclude_folder,
                    "repost_folder": move_folder
                },
            "other": {
                    "delete_mode": delete_mode,
                    "repost_mode": repost_mode,
                }
    }
    toml.dump(toml_dict, CONFIG_TOML)

if __name__ == "__main__":
    main()
