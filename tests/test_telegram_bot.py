from src.core.conf_helper import ConfHelper
from src.core.telegram_bot import TelegramBot
import pytest


@pytest.mark.skip(reason="Starts a bot but cannot stop properly every threads which cause infinite loop, "
                         "use as standalone for testing.")
def test_start_bot():
    conf = "./test_files/test_config_API.toml"
    TelegramBot(ConfHelper.load_conf(conf))
    assert True
