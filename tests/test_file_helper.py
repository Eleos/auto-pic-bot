import logging
import os.path
from src.core.conf import Conf
from src.core.conf_helper import ConfHelper
from src.core.file_helper import FileHelper, MediaType


def setup_module():
    """
    Move all img back into src_folder
    """
    conf = ConfHelper.load_conf("./test_files/test_config.toml")
    for file in os.listdir(conf.repost_folder):
        os.rename(f"{conf.repost_folder}/{file}", f"{conf.src_folder}/{file}")
    for file in os.listdir(conf.exclude_folder):
        os.rename(f"{conf.exclude_folder}/{file}", f"{conf.src_folder}/{file}")


class TestFileHelper:
    CONF: Conf = ConfHelper.load_conf("./test_files/test_config.toml")
    IMG_NUMBER = 4
    VIDEO_NUMBER = 1

    def test_list_media(self):
        assert len(FileHelper.list_media(self.CONF.src_folder)) == self.IMG_NUMBER + self.VIDEO_NUMBER

    def test_list_media_types(self):
        list_media = FileHelper.list_media(self.CONF.src_folder)
        cpt_img = 0
        cpt_video = 0
        videos = filter(lambda x: x[1] == MediaType.ANIMATED.name, list_media)
        for video in videos:
            cpt_video += 1
        assert cpt_video == self.VIDEO_NUMBER

        img = filter(lambda x: x[1] == MediaType.IMG.name, list_media)
        for x in img:
            cpt_img += 1
        assert cpt_img == self.IMG_NUMBER

    def test_move_media(self):
        try:
            media_path = FileHelper.list_media(self.CONF.src_folder)[0][0]
        except IndexError as e:
            logging.error(f"No media found {e}")
            return
        media_name = os.path.basename(media_path)
        FileHelper.move_media(self.CONF.src_folder, self.CONF.repost_folder, media_path)
        assert len(FileHelper.list_media(self.CONF.src_folder)) == self.IMG_NUMBER + self.VIDEO_NUMBER - 1
        os.rename(f"{self.CONF.repost_folder}/{media_name}", f"{self.CONF.src_folder}/{media_name}")
        assert len(FileHelper.list_media(self.CONF.src_folder)) == self.IMG_NUMBER + self.VIDEO_NUMBER
    
    def test_exclude_media_list(self):
        media_long = FileHelper.exclude_medias(self.CONF.src_folder)[0][0]
        assert media_long.__contains__("long.png")
        
    def test_delete_media(self):
        testfile = "./test_files/tmp.txt"
        with open(testfile, "x") as f:
            f.write("test")
            f.close()
        assert os.path.isfile(testfile)
        FileHelper.delete_media(testfile)
        assert not os.path.exists(testfile)
