from src.commands.pictures_left_command import PicturesLeftCommand
from src.core.conf_helper import ConfHelper


def test_cmd_init():
    PATH = "./test_files/test_config.toml"

    conf = ConfHelper.load_conf(PATH)
    cmd = PicturesLeftCommand(conf)
    assert cmd.help_text
