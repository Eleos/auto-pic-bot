from src.commands.help_command import HelpCommand
from src.core.conf import Conf


def test_cmd_init():
    conf = Conf()
    conf.time_interval = 3600
    cmd = HelpCommand(conf, [])
    assert cmd.command_name == "help" and cmd.help_text == "The base help command."
