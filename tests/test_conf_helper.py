import toml
from src.core.conf import Conf
from src.core.conf_helper import ConfHelper


class TestConfHelper:
    PATH = "./test_files/test_config.toml"
    PATH2 = "./test_files/test_config2.toml"

    def test_load_conf(self):
        conf = ConfHelper.load_conf(self.PATH)

        # Test loaded conf content is the same as the file
        assert toml.loads(str(conf)) == toml.load(self.PATH)

    def test_save_conf(self):
        conf = ConfHelper.load_conf(self.PATH)

        # Test saving conf in file then equality between current conf and file
        ConfHelper.save_conf(conf, self.PATH)
        assert str(conf) == str(ConfHelper.load_conf(self.PATH))

    def test_save_edited(self):
        conf = ConfHelper.load_conf(self.PATH)
        conf.token = "Sp8D3R"
        ConfHelper.save_conf(conf, self.PATH2)
        assert str(conf) == str(ConfHelper.load_conf(self.PATH2))
        assert str(ConfHelper.load_conf(self.PATH)) != str(ConfHelper.load_conf(self.PATH2))

    def test_conf_change(self):
        conf = ConfHelper.load_conf(self.PATH)
        conf2 = ConfHelper.load_conf(self.PATH)
        container = ConfContainerClass(conf2)
        assert container.conf.token == conf.token
        conf2.token = "Sp8d3R"
        assert container.conf.token != conf.token


class ConfContainerClass:
    __test__ = False

    def __init__(self, conf: Conf):
        self.__conf = conf

    @property
    def conf(self):
        return self.__conf

    @conf.setter
    def conf(self, value: Conf):
        self.__conf = value
