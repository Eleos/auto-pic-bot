"""This module contains the ConfigException class."""


class ConfigException(Exception):
    """ Raised when the config isn't properly set """
    pass
