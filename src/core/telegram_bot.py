"""This module contains the main class for starting a telegram picture bot."""

import logging
import random

from typing import Union
from telegram.ext import Updater, CommandHandler, CallbackContext

from commands import *
from core.conf import Conf
from core.file_helper import FileHelper, MediaType


class TelegramBot:
    """The main class for a telegram picture bot.

    Args:
        config (Conf): The configuration that'll be used for this bot.
    """

    def __init__(self, config: Conf):
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.INFO)
        self.__logger = logging.getLogger(__name__)

        self.__conf = config
        self.__cmd_list = []
        self.__handlers = []
        self.__updater = Updater(token=self.__conf.token, use_context=True)

        # Add handlers
        self.__cmd_list.append(PicturesLeftCommand(self.__conf))
        self.__cmd_list.append(HelpCommand(self.__conf, self.__cmd_list))

        # Handle and Dispatch commands
        for cmd in self.__cmd_list:
            handler = CommandHandler(cmd.command_name, cmd.action)
            self.__handlers.append(handler)
            self.__updater.dispatcher.add_handler(handler)

        # Run filter of old files at startup
        self.__updater.job_queue.run_once(self.filter_media, 1, context=self.__conf)

        # Run job once then every x time
        self.__updater.job_queue.run_repeating(self.post_media, interval=self.__conf.time_interval, first=5,
                                               context=self.__conf)
        self.__updater.start_polling()
        self.__updater.idle()

    def filter_media(self, context: CallbackContext):
        """Filter every media currently stocked and move error medias/files to the exclude folder"""
        # Then filter medias that are in the moved folder, the post media function will check the source folder everytime before posting
        repost_excludes = FileHelper.exclude_medias(self.__conf.repost_folder)
        for repost_media in repost_excludes:
            FileHelper.move_media(self.__conf.repost_folder, self.__conf.exclude_folder, repost_media)

    def post_media(self, context: CallbackContext):
        """Post a media in the channel given in the configuration file.

        Args:
            context (CallbackContext): The context from which this method was called.
        """
        # Exclude wrong media from the source folder
        src_excludes = FileHelper.exclude_medias(self.__conf.src_folder)
        for src_f in src_excludes:
            FileHelper.move_media(self.__conf.src_folder, self.__conf.exclude_folder, src_f)

        (file_type, file_path) = self.get_media()
        if file_type is not None and file_path is not None:
            logging.info("Posting media on channel...")
            with open(file_path, "rb") as f :
                if file_type == MediaType.IMG.name:
                    context.bot.send_photo(self.__conf.chat_id, f)
                elif file_type == MediaType.ANIMATED.name:
                    context.bot.send_animation(self.__conf.chat_id, f)
                elif file_type == MediaType.VIDEO.name:
                    context.bot.send_video(self.__conf.chat_id, f)
            logging.info("Posted media in channel '%s'.", self.__conf.chat_id)
            if self.__conf.repost_mode:
                FileHelper.move_media(self.__conf.src_folder, self.__conf.repost_folder, file_path)
            elif self.__conf.delete_mode:
                FileHelper.delete_media(file_path)
        else:
            logging.error("Couldn't post media, check logs for more informations.")

    def get_media(self) -> Union[tuple[None, None], tuple[MediaType, str]]:
        """Pick a media and returns a buffered reader containing the raw data for the picked media.

        Returns:
            tuple[None, None]: Returns an empty tuple when no files where found.
            tuple[MediaType, str]: Returns a tuple containing the type of the media and its path.
        """
        try:
            medias = FileHelper.list_media(self.__conf.src_folder)
            if len(medias) == 0 and self.__conf.repost_mode:
                medias = FileHelper.list_media(self.__conf.repost_folder)
            (media_path, media_type) = random.choice(medias)
            logging.info("Found media : '%s'", media_path)
        except IndexError as e:
            logging.error("No media found %s", e)
            return None, None
        except OSError as e:
            logging.error("Could not read media %s", e)
            return None, None
        return media_type, media_path
