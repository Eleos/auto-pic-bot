"""This module contains the ConfHelper class that gives access to helpers methods for translating configuration
between a toml file and a Conf object. """

import logging
import sys
from pathlib import Path

import toml

from core.conf import Conf
from core.config_exception import ConfigException


class ConfHelper:
    """A utility class to manage configuration loading and saving from a given path."""

    @staticmethod
    def load_conf(path: str):
        """Returns a configuration object initialised from the data in the given config file path.

        Args:
            path (str): The path to the configuration file.

        Returns:
            Conf: Generate and verify a Conf object based on the given file.
        """
        try:
            p = Path(path)
            if not (p.exists() and p.is_file()):
                raise ConfigException("{} does not exist or isn't a file.".format(p))
            with open(p) as f:
                config_raw = toml.load(f)
                f.close()
            config = Conf()
            config.token = config_raw["telegram_API"]["token"]
            config.chat_id = config_raw["telegram_API"]["chat_id"]
            config.time_interval = config_raw["telegram_API"]["time_interval"]
            config.src_folder = config_raw["paths"]["src_folder"]
            config.exclude_folder = config_raw["paths"]["exclude_folder"]
            config.repost_folder = config_raw["paths"]["repost_folder"]
            config.delete_mode = config_raw["other"]["delete_mode"]
            config.repost_mode = config_raw["other"]["repost_mode"]
            if not config.validate():
                raise ConfigException("{} is missing required parameters.".format(path))
        except (ConfigException or OSError) as e:
            logging.critical("Error in config: {}".format(e))
            sys.exit(1)
        return config

    @staticmethod
    def save_conf(conf: Conf, path: str):
        """Save the given conf into the given file path.

        Args:
            conf (Conf): The configuration object that'll be saved into the file.
            path (str): The path to which the configuration will be writen in.

        Returns:
            int: 0 if the saving happened without problems, -1 if an error occurred.
        """
        try:
            p = Path(path)
            with open(p, "w") as f:
                f.write(str(conf))
                f.close()
        except ConfigException as e:
            logging.error("Error while saving: {}".format(e))
            return -1
        return 0
