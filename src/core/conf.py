"""This module contains the class of Conf objects."""


class Conf:
    """The base configuration object that helps manage the configs content. Initialize with a default invalid config.
    """

    def __init__(self):
        self._token = ""
        self._chat_id = -1
        self._time_interval = -1
        self._src_folder = ""
        self._repost_folder = ""
        self._exclude_folder = ""
        self._delete_mode = False
        self._repost_mode = True

    @property
    def token(self):
        """The telegram API Token."""
        return self._token

    @token.setter
    def token(self, value: str):
        self._token = value

    @token.deleter
    def token(self):
        del self._token

    @property
    def chat_id(self):
        """The id of the channel the bot will post pictures in."""
        return self._chat_id

    @chat_id.setter
    def chat_id(self, value: int):
        self._chat_id = value

    @chat_id.deleter
    def chat_id(self):
        del self.chat_id

    @property
    def time_interval(self):
        """The time the bot must wait between each image posting (In seconds)."""
        return self._time_interval

    @time_interval.setter
    def time_interval(self, value: int):
        self._time_interval = value

    @time_interval.deleter
    def time_interval(self):
        del self._time_interval

    @property
    def src_folder(self):
        """The source folder from which new pictures will be taken."""
        return self._src_folder

    @src_folder.setter
    def src_folder(self, value: str):
        self._src_folder = value

    @src_folder.deleter
    def src_folder(self):
        del self._src_folder

    @property
    def exclude_folder(self):
        """The folder to which invalid medias in the src folder will be moved to"""
        return self._exclude_folder

    @exclude_folder.setter
    def exclude_folder(self, value: str):
        self._exclude_folder = value
    
    @exclude_folder.deleter
    def exclude_folder(self):
        del self._exclude_folder

    @property
    def repost_folder(self):
        """The folder to which posted pictures will be moved to.

        If *repost_mode* is set to True;
        the content of the repost folder will be used when the source folder is empty."""
        return self._repost_folder

    @repost_folder.setter
    def repost_folder(self, value: str):
        self._repost_folder = value

    @repost_folder.deleter
    def repost_folder(self):
        del self._repost_folder

    @property
    def delete_mode(self):
        """Whenever the bot is running in delete mode or not. Delete mode means the bot will delete posted images
        from the source folder.

        Keep in mind that if both *repost_mode* and *delete_mode* are set to true,
        the delete mode will be ignored. """
        return self._delete_mode

    @delete_mode.setter
    def delete_mode(self, value: bool):
        self._delete_mode = value

    @delete_mode.deleter
    def delete_mode(self):
        del self._delete_mode

    @property
    def repost_mode(self):
        """If true, allows the bot to repost pictures once the source folder is empty."""
        return self._repost_mode

    @repost_mode.setter
    def repost_mode(self, value: bool):
        self._repost_mode = value

    @repost_mode.deleter
    def repost_mode(self):
        del self._repost_mode

    def validate(self):
        """Simply checks that all values are set and valid

        Returns:
            bool: True if the values are valid. Other checks are done at runtime by raising corresponding errors.
        """
        if self.token != "" and \
                self.chat_id != -1 and \
                self.time_interval != -1 and \
                self.src_folder != "" and \
                self.exclude_folder != "" and \
                (self.repost_mode and self.repost_folder != ""):
            return True
        return False

    def __str__(self):
        return """[telegram_API]
token = "{}"
chat_id = {}
time_interval = {}

[paths]
src_folder = "{}"
exclude_folder = "{}"
repost_folder = "{}"

[other]
delete_mode = {}
repost_mode = {}
""".format(self.token, self.chat_id, self.time_interval, self.src_folder, self.exclude_folder, self.repost_folder,
           str(self.delete_mode).lower(), str(self.repost_mode).lower())
