"""This module contains the class FileHelper that contains useful tools to manage media and files for the
TelegramBot. """
import logging
import os
import functools
from enum import Enum

import telegram
from PIL import Image

import filetype

MAX_PHOTOSIZE_UPLOAD = telegram.constants.MAX_PHOTOSIZE_UPLOAD
MAX_HEIGH_WIDTH = 10000
MAX_FILESIZE_UPLOAD = telegram.constants.MAX_FILESIZE_UPLOAD
MAX_VIDEOSIZE_UPLOAD = telegram.constants.MAX_FILESIZE_UPLOAD

class MediaType(Enum):
    """The different types of media accepted as images and animations."""
    #TODO: Get the constants from the telegram.constants v20
    IMG = (["image/jpeg", "image/png", "image/webp"],
    [
        lambda p: os.path.getsize(p) < MAX_PHOTOSIZE_UPLOAD,
        lambda p: sum(Image.open(p).size) < MAX_HEIGH_WIDTH,
        lambda p: 1/20.0 <= (lambda t: t[0] / float(t[1]))(Image.open(p).size) <= 20
    ])
    ANIMATED = (["image/gif"],
    [
        lambda p: os.path.getsize(p) < MAX_FILESIZE_UPLOAD
    ])
    VIDEO = (["video/mp4", "video/webm"],
    [
        lambda p: os.path.getsize(p) < MAX_VIDEOSIZE_UPLOAD
    ])
    OTHER = None


class FileHelper:
    """A class that helps to manage media and files for the bot."""

    @staticmethod
    def move_media(from_folder: str, to_folder: str, media_name: str):
        """
        Move the given media from the source folder to repost folder.

        Args:
            from_folder (str) : Folder name to transfer from
            to_folder (str) : Folder name to transfer to the file
            media_name (str): The name of the media that'll be moved.
        """
        if os.path.isfile(f"{from_folder}/{os.path.basename(media_name)}"):
            os.rename(f"{from_folder}/{os.path.basename(media_name)}",
                      f"{to_folder}/{os.path.basename(media_name)}")

    @staticmethod
    def delete_media(media_path: str):
        """Delete the given media."""
        if os.path.isfile(media_path):
            os.remove(media_path)

    @staticmethod
    def exclude_medias(path: str) -> list[str]:
        """Return files that doesn't fit into one of the compatible media (see MediaType class)"""
        res = FileHelper.__filter_media(path, os.listdir(path), additional_filter=True, invert=True)
        return [a[0] for a in res]

    @staticmethod
    def list_media(path: str) -> list[tuple[str, MediaType]]:
        """List all compatible media (See MediaType class)

        Args:
            path (str): The path to the folder from which we list the media.

        Returns:
            list[tuple[str, MediaType]]: Returns a list of available media, either from the source folder or from the
                repost folder in case the source folder is empty.
        """
        # Get file list
        res = FileHelper.__filter_media(path, os.listdir(path))
        return res

    @staticmethod
    def __filter_media(base_path: str, file_list: list[str], additional_filter=False, invert=False) -> list[tuple[str, MediaType]]:
        """Inner function that filters out media from the given folder according to the accepted mime types in
        MediaType.

        Args:
            base_path (str): The base path in which we will filter out unknown/unwanted media types
                (see MediaType class)
            file_list (list[str]): list of the files to filter
            additional_filter: optionnal parameter set to False, slower but more advanced filters (see lambdas in MediaType)
            invert: optionnal parameter set to False, invert the returned value
        """
        res = []
        if invert:
            invert_res = []

        # Filter files category and exclude unknown formats/folders
        for file in file_list:
            file_path = f"{base_path}/{file}"
            if os.path.isfile(file_path):
                # We made this because match isn't in python 3.9 and debian is old
                m = filetype.guess(file_path)
                if m.mime in MediaType.IMG.value[0]:
                    if additional_filter:
                        if functools.reduce(lambda x, y: x and y, [filter(file_path) for filter in MediaType.IMG.value[1]]):
                            res.append((file_path, MediaType.IMG.name))
                        elif invert:
                            invert_res.append((file_path, MediaType.IMG.name))
                    else:
                        res.append((file_path, MediaType.IMG.name))
                elif m.mime in MediaType.ANIMATED.value[0]:
                    if additional_filter:
                        if functools.reduce(lambda x, y: x and y, [filter(file_path) for filter in MediaType.ANIMATED.value[1]]):
                            res.append((file_path, MediaType.ANIMATED.name))
                        elif invert:
                            invert_res.append((file_path, MediaType.ANIMATED.name))
                    res.append((file_path, MediaType.ANIMATED.name))
                elif m.mime in MediaType.VIDEO.value[0]:
                    if additional_filter:
                        if functools.reduce(lambda x, y: x and y, [filter(file_path) for filter in MediaType.VIDEO.value[1]]):
                            res.append((file_path, MediaType.VIDEO.name))
                        elif invert:
                            invert_res.append((file_path, MediaType.VIDEO.name))
                    res.append((file_path, MediaType.VIDEO.name))
                elif m is None:
                    logging.warning("Cannot guess mime type of file at '%s'. ", file_path)
                    if invert:
                        invert_res.append((file_path, MediaType.OTHER.name))
                else:
                    logging.warning("Unwanted file type at '%s'. ", file_path)
                    logging.warning("Guessed mime type was '%s'.", m.mime)
                    if invert:
                        invert_res.append((file_path, MediaType.OTHER.name))
        if invert:
            return invert_res
        return res
