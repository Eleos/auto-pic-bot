"""This module contains the class of the '/help' command."""

from telegram import Update
from telegram.ext import CallbackContext

from commands.abstract_command import AbstractCommand
from core.conf import Conf


class HelpCommand(AbstractCommand):
    """The help command. Displays the interval at which a picture is posted and a list of registered commands via the
    help of their 'name' and 'help_text' attributes.

    Args:
        config (Conf): The config from which we can get the time interval.
        cmd_list (list[AbstractCommand]): The list of commands from which
            we will create the help message.

    Attributes:
        __conf (Conf): Private attribute to access the running configuration.
        __cmd_list (list[AbstractCommand]): Private attribute to access the list of all registered commands.
    """

    def __init__(self, config: Conf, cmd_list: list[AbstractCommand]):
        super().__init__("help", "The base help command.")
        self.__conf = config
        self.__cmd_list = cmd_list

    def action(self, update: Update, context: CallbackContext):
        """List all commands and their respective 'help_text' and send it back to the user who issued the help command.

        Args:
            update (Update): An Update object given when the command is invoked as a callback.
            context (CallbackContext): The context from which the callback happened.
        """
        txt = "This the YuriPostPicBot, bot who posts pictures " \
              f"every {str(int(self.__conf.time_interval) / 60)} minutes.\n "
        for cmd in self.__cmd_list:
            txt += f"\n/{cmd.command_name} : {cmd.help_text}"
        context.bot.send_message(chat_id=update.effective_chat.id, text=txt)
