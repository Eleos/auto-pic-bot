"""This module stores all commands and their functionalities."""

from .help_command import HelpCommand
from .pictures_left_command import PicturesLeftCommand
