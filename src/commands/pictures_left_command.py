# noinspection SpellCheckingInspection
"""This module contains the class of the '/pleft' command."""

from telegram import Update
from telegram.ext import CallbackContext

from commands.abstract_command import AbstractCommand
from core.conf import Conf
from core.file_helper import FileHelper


class PicturesLeftCommand(AbstractCommand):
    """A command that lists remaining pics in src folder.

    Args:
        config (Conf): The config from which we can get the source folder path.

    Attributes:
        __conf (Conf): Private attribute to access the running configuration.
    """

    def __init__(self, config: Conf):
        super().__init__("pleft", "Show the number of pictures left in source folder (Does not take the repost folder "
                                  "into account).")
        self.__conf = config

    def action(self, update: Update, context: CallbackContext):
        """Reports the number of pictures left in source and repost folders (If repost mode is set)
        to the issuer of the command.

        Args:
            update (Update): An Update object given when the command is invoked as a callback.
            context (CallbackContext): The context from which the callback happened.
        """
        msg = f"{len(FileHelper.list_media(self.__conf.src_folder))} pictures left in source folder."
        if self.__conf.repost_mode:
            msg += f"\n{len(FileHelper.list_media(self.__conf.repost_folder))} pictures in repost folder."

        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=msg)
