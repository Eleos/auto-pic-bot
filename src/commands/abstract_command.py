"""This module contains the abstraction for all telegram bot commands."""

from abc import ABC, abstractmethod

from telegram import Update
from telegram.ext import CallbackContext


class AbstractCommand(ABC):
    """A generic abstract method for telegram commands.

    Args:
        name (str): The name of the command that'll be used (ex: if this is set to 'help',
            the command action will be called by typing '/help')
        help_text (str): The text that describes what this command does.
    """

    def __init__(self, name: str, help_text: str):
        self._help_text = help_text
        self._command_name = name

    # Getters, setters and deleters

    @property
    def help_text(self) -> str:
        """The help text that describes the command."""
        return self._help_text

    @help_text.setter
    def help_text(self, value: str):
        self._help_text = value.lower()

    @help_text.deleter
    def help_text(self):
        del self._help_text

    @property
    def command_name(self) -> str:
        """The command name."""
        return self._command_name

    @command_name.setter
    def command_name(self, value: str):
        self._command_name = value.lower()

    @command_name.deleter
    def command_name(self):
        del self._command_name

    @abstractmethod
    def action(self, update: Update, context: CallbackContext):
        """The action that'll be done by the command.

        Args:
            update (Update): An Update object given when the command is invoked as a callback.
            context (CallbackContext): The context from which the callback happened.
        """
        pass
