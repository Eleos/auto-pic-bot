"""This module contains the starting point for telegram picture bot and also manages command lines through the
click module"""

import click

from core.conf_helper import ConfHelper
from core.telegram_bot import TelegramBot


@click.command(help="A simple telegram bot that posts pictures.")
@click.option("-c", "--conf", "conf",
              default="config.toml",
              show_default=True,
              help="The path to the configuration file.")
def main(conf: str):
    """Start the bot with the given configuration path

    Args:
        conf (str): The path to the configuration file (Defaults to './config.toml').
    """
    TelegramBot(ConfHelper.load_conf(conf))


if __name__ == "__main__":
    main()
