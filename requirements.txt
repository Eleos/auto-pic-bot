python-telegram-bot==13.15
click==8.1.2
pytest==7.1.1
requests==2.27.1
filetype==1.2.0
toml==0.10.2
pillow==10.2.0